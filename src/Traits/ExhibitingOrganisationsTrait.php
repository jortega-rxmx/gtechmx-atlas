<?php

namespace Gtechmx\Atlas\Traits;

trait ExhibitingOrganisationsTrait
{
    protected static function getExhibitingOrganisations($eventEditionId, $query)
    {

        if (!$query) {
            return self::fullExhibitingOrganisationsQuery($eventEditionId);
        }

        return $query;
    }

    private static function fullExhibitingOrganisationsQuery($eventEditionId)
    {
        return '{
  exhibitingOrganisations(
    eventEditionId: "'.$eventEditionId.'"
  ){
    accompanyingWebsiteUrl
    addressLine1
    addressLine2
    city
    companyName
    contactEmail
    countryCode
    coverImageUrl
    description
    displayName
    documents{
      displayName
      documentType
      exhibitingOrganisationId
      id
      publishDate
      size
      url
    }
    entitlements{
      productCode
      totalValue
    }
    eventEditionId
    exhibitorStatus
    exhibitorType
    extraCharacteristics
    features
    filterCategories{
      questionId
      responses{
        answerId
        taxonomyId
      }
    }
    id
    isNew
    locale
    logoUrl
    organisationId
    package
    phone
    postcode
    profileQuestions{
      questionId
      responses{
        answerId
        taxonomyId
      }
    }
    representedBrands{
      name
    }
    representedBrands{
      name
    }
    sessions{
      description
      endTime
      id
      location
      sessionDate
      sessionSpeakers{
        name
      }
      startTime
      title
    }
    showObjective
    socialMedia{
      name
      url
    }
    sortAlias
    stands{
      name
    }
    stateProvince
    website
  }
}';
    }
}
